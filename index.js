const electron = require('electron');
const {app} = electron;

const {BrowserWindow} = electron;

let win;

function createWindow() {
    win = new BrowserWindow({
        title: 'Sentinel by Conveyant',
        icon: __dirname + '/Content/favicon.ico'
    });

    // Disables the default menu
    // TODO Replace with custom menu
    win.setMenu(null);

    // Load initial html file
    win.loadURL(`file://${__dirname}/index.html`);

    win.on('closed', () => {
      win = null;
    });
}

app.on('ready', createWindow);

app.on('window-all-closed', () => {
    if (process.platform !== 'darwin') {
        app.quit();
    }
});


app.on('activate', () => {
    if (win === null) {
        createWindow();
    }
});
